//type Pop<T extends unknown[]> = NonNullable<{[K in keyof T]: K extends "2" ? null : T[K]}>; 
//type Pop<T extends unknown[]> = T[number] extends infer A ? A : 0
type Pop<T extends unknown[]> = {[K in keyof T]: K extends [never,...T]['length'] ? never : T[K]}

type BBB = Pop<[3, 2, 1]>