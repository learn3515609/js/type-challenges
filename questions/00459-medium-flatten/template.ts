type Flatten<T extends any[], A extends any[] = []> = T extends [infer X, ...infer Y] ? (X extends any[] ? Flatten<[...X, ...Y], A> : Flatten<[...Y], [...A, X]>) : A //

type FFFF = Flatten<[[1, 5], 2, [3, 4], [[[5]]]]>

