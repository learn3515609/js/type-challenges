type AppendToObject<T extends Object, U extends string, V extends any> = {
    [K in keyof T | U]: K extends keyof T ? T[K] : V;
} & {
}