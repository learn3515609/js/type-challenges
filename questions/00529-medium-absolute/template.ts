type Absolute<T extends number | string | bigint> =  `${T}` extends `-${infer N}` ? N : `${T}`;


type Test = -5
type Result = Absolute<Test> // expected to be "100"