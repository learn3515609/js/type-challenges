type Space = ' ' | '\n\t' | '\t'
type Trim<S extends string> = S extends `${Space}${infer SS1}` ? Trim<SS1> : S extends `${infer SS2}${Space}` ? Trim<SS2> : S;