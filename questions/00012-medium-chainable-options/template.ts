type Chainable<O extends Object = {}> = {
  option<K extends string, V extends unknown>(key: K extends keyof O ? never : K, value: V): Chainable<
    (K extends keyof O ? Omit<O, K> : O ) & Record<K, V>
  >
  get(): O
}
