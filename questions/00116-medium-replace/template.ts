type Replace<S extends string, From extends string, To extends string> = From extends '' ? S : 
    S extends `${infer SS1}${From}${infer SS2}` ? `${SS1}${To}${SS2}` : S