type RestString110 = string;
type MyCapitalize<S extends string> = S extends `${infer F}${infer Tail}` ? `${Uppercase<F>}${Tail}` : S;


type T = MyCapitalize<'foo bar'>