//type MyAwaited<T extends {then: Function}> = T extends {then: (onfulfilled: (arg: infer A) => any) => any} ? (A extends Promise<any> ? MyAwaited<A> : A) : never;

type MyAwaited<T extends PromiseLike<any>> = T extends PromiseLike<infer A> ? (A extends PromiseLike<any> ? MyAwaited<A> : A) : never;

type a = MyAwaited<{ then: (onfulfilled: (arg: number) => any) => any }>