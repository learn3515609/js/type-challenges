type Last<T extends any[]> = [never, ...T][T['length']]

type TTT = Last<[3, 2, 1]>