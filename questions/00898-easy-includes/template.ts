type Includes<T extends readonly any[], U> = T extends U ? true : false;

type c = Includes<['Kars', 'Esidisi', 'Wamuu', 'Santana'], 'Kars'>;