type DeepReadonly<T> = {
    readonly [P in keyof T]: T[P] extends object ? DeepReadonly<T[P]> : T[P]
}



    type X1 = {
        a: () => 22
        b: string
        c: {
          d: boolean
          e: {
            g: {
              h: {
                i: true
                j: 'string'
              }
              k: 'hello'
            }
            l: [
              'hi',
              {
                m: ['hey']
              },
            ]
          }
        }
      }
      
      type X2 = { a: string } | { b: number }
      

    type P1 = DeepReadonly<X1>
    type P2 = DeepReadonly<X2>


    type Expected1 = {
        readonly a: () => 22
        readonly b: string
        readonly c: {
          readonly d: boolean
          readonly e: {
            readonly g: {
              readonly h: {
                readonly i: true
                readonly j: 'string'
              }
              readonly k: 'hello'
            }
            readonly l: readonly [
              'hi',
              {
                readonly m: readonly ['hey']
              },
            ]
          }
        }
      }

      Equal<DeepReadonly<X1>, Expected1>